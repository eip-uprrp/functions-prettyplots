//************************************
// By Ive 4, June 4                  *
//************************************

#include<iostream>
#include "xyplotwindow.h"
#include <QApplication>
using namespace std;

//**********************************************************
// This function is to illustrate the difference between   *
// pass by value and pass by reference                     *
//**********************************************************


void illustration(int paramValue, int &paramRef)
{
    paramValue=1;
    paramRef=1;
    cout << endl << "The content of paramValue is: " << paramValue << endl
         << "The content of paramRef is: " << paramRef << endl;
}

//************************************
// Functions circle                  *
//************************************

void circle(double p, double &xCoord, double &yCoord)
{
    xCoord = 5 * cos(p);
    yCoord = 5 * sin(p);
}

//Overloaded Funcion circle

// YOUR CODE HERE


//************************************
// Function butterfly                *
//************************************

// YOUR CODE HERE


//***************
// Function main *
//***************


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    XYPlotWindow wCircleR5;
    XYPlotWindow wCircle;
    XYPlotWindow wButterfly;

    
    double r;
    double y = 0.00;
    double x = 0.00;
    double increment = 0.01;
    int argValue=0, argRef=0;

    // invoke the function illustration to view the contents of variables by value and by reference

        illustration(argValue,argRef);
        cout << endl << "The content of argValue is: " << argValue << endl
             << "The content of argRef is: " << argRef << endl;



    // repeat for several values of the angle t
    for (double t = 0; t < 16*M_PI; t = t + increment)
    {
        
        // invoke circle with the angle t and reference variables x, y as arguments
        circle(t,x,y);
        
        // add the point (x,y) to the graph of the circle
        wCircleR5.AddPointToGraph(x,y);
        
        
        // invoke circle with the radius r, the angle t and reference variables x, y as arguments
      
        // YOUR CODE HERE
        
        // add the point (x,y) to the graph of the circle
       
        // YOUR CODE HERE

        
        // invoke butterfly with the angle t and reference variables x, y as arguments
      
        // YOUR CODE HERE

        // add the point (x,y) to the graph of the butterfly
       
        // YOUR CODE HERE
    }

    // After all the points have been added, plot and show the graphs
    
    wCircleR5.Plot();
    wCircleR5.show();
    
    // YOUR CODE HERE
    

    return a.exec();
}
